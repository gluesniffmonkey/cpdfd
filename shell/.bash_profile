#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc


GUIX_PROFIL=E"$HOME/.guix-profile"
. "$GUIX_PROFILE/etc/profile"

VISUAL='emacsclient -t'
export VISUAL
. "$HOME/.cargo/env"
