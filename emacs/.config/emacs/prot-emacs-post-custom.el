;;; God Mode begins
;; First, ensure god-mode is installed
(use-package god-mode
  :custom
  (god-exempt-major-modes nil)
  (god-exempt-predicates nil)
  :config
  ;; Enable god-mode using escape
  (global-set-key (kbd "<escape>") #'god-local-mode)
  
  ;; Change cursor color when god-mode is active
  (defun my-god-mode-update-cursor-type ()
    (setq cursor-type (if (or god-local-mode buffer-read-only) 'box 'bar)))
  
  (defun my-god-mode-update-cursor-color ()
    (set-cursor-color (if god-local-mode "#FFD700" "#ffffff")))  ; Gold color in god-mode
  
  (add-hook 'god-mode-enabled-hook #'my-god-mode-update-cursor-type)
  (add-hook 'god-mode-disabled-hook #'my-god-mode-update-cursor-type)
  (add-hook 'god-mode-enabled-hook #'my-god-mode-update-cursor-color)
  (add-hook 'god-mode-disabled-hook #'my-god-mode-update-cursor-color)

  ;; Integration with which-key
  (defun my-god-mode-which-key-update ()
    (which-key-add-key-based-replacements
      "g" "god-mode"
      "i" "insert-mode"))
  
  (add-hook 'god-mode-enabled-hook #'my-god-mode-which-key-update)

  ;; Keep your existing which-key configuration
  (use-package which-key
    :config
    (which-key-mode)
    :custom
    (which-key-max-description-length 40)
    (which-key-lighter nil)
    (which-key-sort-order 'which-key-description-order))

  ;; Additional key bindings for god-mode
  (define-key god-local-mode-map (kbd ".") #'repeat)
  (define-key god-local-mode-map (kbd "i") #'god-local-mode)
  
  ;; Better handling of C-c sequences in god-mode
  (defun my-god-mode-lookup-command-advice (lookup-fn key)
    "Translate any C-c sequence to its god-mode equivalent."
    (let ((command (funcall lookup-fn key)))
      (if (and (symbolp command)
               (string-prefix-p "C-c" (key-description key)))
          (let* ((key-seq (key-description key))
                 (translated-seq (replace-regexp-in-string "C-c" "c" key-seq t))
                 (translated-key (kbd translated-seq)))
            (or (lookup-key god-local-mode-map translated-key)
                command))
        command)))
  
  (advice-add 'god-mode-lookup-command :around #'my-god-mode-lookup-command-advice)

  ;; Enable god-mode for all buffers
  (god-mode))
