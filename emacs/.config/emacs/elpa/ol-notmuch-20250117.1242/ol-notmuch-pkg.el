;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "ol-notmuch" "20250117.1242"
  "Links to notmuch messages."
  '((emacs   "29.1")
    (compat  "30.0.2.0")
    (notmuch "0.38.2")
    (org     "9.7.19"))
  :url "https://github.com/tarsius/ol-notmuch"
  :commit "9a69506a3f9ed31e2f1f967dfaa600702089be45"
  :revdesc "9a69506a3f9e"
  :keywords '("hypermedia" "mail")
  :authors '(("Matthieu Lemerre" . "racin@free.fr"))
  :maintainers '(("Jonas Bernoulli" . "emacs.ol-notmuch@jonas.bernoulli.dev")))
