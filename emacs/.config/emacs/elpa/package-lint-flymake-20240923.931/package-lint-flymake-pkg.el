;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "package-lint-flymake" "20240923.931"
  "A package-lint Flymake backend."
  '((emacs        "26.1")
    (package-lint "0.5"))
  :url "https://github.com/purcell/package-lint"
  :commit "e3d3fb254221d053c75edfd9f0ebfa58d1eb52f9"
  :revdesc "e3d3fb254221")
